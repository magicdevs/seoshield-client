<?php
/*
 *  Cache Module
 */
if(!defined("SEOSHIELD_ROOT_PATH"))define("SEOSHIELD_ROOT_PATH",rtrim(realpath(dirname(__FILE__)),"/"));

class SeoShieldModule_bot_cache extends SeoShieldModule
{
    private $robotsPath;
    private $cacheDir = SEOSHIELD_ROOT_PATH.'/data/html_cache/';
    private $cacheFilePath = '';
    private $createCache = false;
    private $userAgents = array('chrome-lighthouse', 'bot');
    private $maxCacheLifetime = 108000;

    public function __construct()
    {
        $this->robotsPath = $_SERVER["DOCUMENT_ROOT"] . '/robots.txt';
        if (isset($GLOBALS['SEOSHIELD_CONFIG']['bot_cache'])){
            if (isset($GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['user_agent_selectors']) && is_array($GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['user_agent_selectors']) && !empty($GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['user_agent_selectors'])){
                $this->userAgents = $GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['user_agent_selectors'];
            }
            if (isset($GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['cache_dir']) && is_array($GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['cache_dir']) && !empty($GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['cache_dir'])){
                $this->cacheDir = $GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['cache_dir'];
            }
            if (isset($GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['max_cache_lifetime']) && is_array($GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['cache_dir']) && !empty($GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['max_cache_lifetime'])){
                $this->maxCacheLifetime = $GLOBALS['SEOSHIELD_CONFIG']['bot_cache']['max_cache_lifetime'];
            }
        }
    }

    function start_cms()
    {
        $user_agent = mb_strtolower($_SERVER["HTTP_USER_AGENT"], 'utf-8');
        if (preg_match('#('.implode('|', $this->userAgents).')#', $user_agent) && (!isset($_SERVER['HTTP_CF_IPCOUNTRY']) || (isset($_SERVER['HTTP_CF_IPCOUNTRY']) && $_SERVER['HTTP_CF_IPCOUNTRY'] != 'UA'))){
            $cachePath = $this->createCachePath($_SERVER["REQUEST_URI"]);
            $cacheFilePath = $this->cacheDir . $cachePath;
            if (file_exists($cacheFilePath) && time() - filemtime($cacheFilePath) < $this->maxCacheLifetime){
                $cacheContent = file_get_contents($cacheFilePath);
                $cacheContent = gzuncompress($cacheContent);
                $cacheContent = str_replace('</body>', '<!--ss_cached_page--></body>', $cacheContent);
                echo $cacheContent;
                exit();
            } else {
                $cacheDir = dirname($cacheFilePath);
                if (!is_dir($cacheDir)){
                    mkdir($cacheDir, 0777, true);
                }
                $this->cacheFilePath = $cacheFilePath;
                $this->createCache = true;
            }
        }
    }

    function html_out_buffer($out_html)
    {
        if ($this->createCache && !empty($this->cacheFilePath) && $this->is_valid_url($out_html)){
            file_put_contents($this->cacheFilePath, gzcompress($out_html,9));
        }

        return $out_html;
    }

    private function is_valid_url($out_html){
        if (function_exists('http_response_code')){
            if (http_response_code() == 404){
                return false;
            }
        }
        if (preg_match('#<meta[^>]*?name=[\'\"]?robots[\'\"]?[^>]*?content=[\'\"]?noindex,\s*?nofollow[\'\"]?[^>]*?>#s', $out_html)){
            return false;
        }
        
        preg_match_all('#<meta[^>]+name=[\'\"]?robots[\'\"]?[^>]*?>#', $out_html, $metaRobots);
        if (isset($metaRobots[0]) && !empty($metaRobots[0]) && is_array($metaRobots[0])){
            foreach ($metaRobots[0] as $metTag) {
                if (strpos($metTag, 'noindex') !== false){
                    return false;
                }
            }
        }

        if (class_exists('RobotsTxtParser\RobotsTxtParser') && class_exists('RobotsTxtParser\RobotsTxtValidator')){
            if (!file_exists($this->robotsPath)){
                return true;
            }
            $parser = new RobotsTxtParser\RobotsTxtParser(file_get_contents($this->robotsPath));
            $url = $GLOBALS["SEOSHIELD_CONFIG"]['page_uri'];
            if (!isset($_SERVER["HTTP_USER_AGENT"]) || empty($_SERVER["HTTP_USER_AGENT"])){
                return true;
            }
            $userAgent = $_SERVER["HTTP_USER_AGENT"];

            $validator = new RobotsTxtParser\RobotsTxtValidator($parser->getRules());
            return $validator->isUrlAllow($url, $userAgent);
        }

        return true;
    }

    private function createCachePath($uri){
        $md5 = md5($uri);
        $cachePath = implode('/', array(substr($md5, -6, -4), substr($md5, -4, -2), substr($md5, -2), $md5)).'.cache.gz';
        return $cachePath;
    }
}

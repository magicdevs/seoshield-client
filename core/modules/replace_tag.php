<?php
/*
* модуль заменяет определённые теги на <span> с соответствующим классом
*/

if (!defined('SEOSHIELD_ROOT_PATH')) {
    define('SEOSHIELD_ROOT_PATH', rtrim(realpath(dirname(__FILE__)), '/'));
}

class SeoShieldModule_replace_tag extends seoShieldModule
{
    public function html_out_buffer($out_html)
    {
        if (!empty($GLOBALS['SEOSHIELD_CONFIG']['replace_tag'])) {
            if (isset($GLOBALS['SEOSHIELD_CONFIG']['replace_tag_disabled_pages']) && !empty($GLOBALS['SEOSHIELD_CONFIG']['replace_tag_disabled_pages']) && in_array($GLOBALS['SEOSHIELD_CONFIG']['page_uri'], $GLOBALS['SEOSHIELD_CONFIG']['replace_tag_disabled_pages'])){
                return $out_html;
            }
            if (isset($GLOBALS['SEOSHIELD_CONFIG']['use_latest_version']) && $GLOBALS['SEOSHIELD_CONFIG']['use_latest_version']){
                // новая версия
                $replace_everywhere = true;
                $is_target_page = false;
                if (isset($GLOBALS['SEOSHIELD_CONFIG']['replace_only_on_specific_pages']) && $GLOBALS['SEOSHIELD_CONFIG']['replace_only_on_specific_pages']){
                    if (!empty($GLOBALS['SEOSHIELD_CONFIG']['replace_tag_page_types'])){
                        foreach ($GLOBALS['SEOSHIELD_CONFIG']['replace_tag_page_types'] as $page_comment) {
                            if (strpos($out_html, $page_comment) !== false){
                                $is_target_page = true;
                                break;
                            }
                        }
                    }
                    if (isset($GLOBALS['SEOSHIELD_CONFIG']['replace_tag_per_page_array']) && !empty($GLOBALS['SEOSHIELD_CONFIG']['replace_tag_per_page_array'])){
                        foreach ($GLOBALS['SEOSHIELD_CONFIG']['replace_tag_per_page_array'] as $single_uri) {
                            if ($GLOBALS['SEOSHIELD_CONFIG']['page_uri'] == $single_uri){
                                $is_target_page = true;
                                break;
                            }
                        }
                    }
                    $replace_everywhere = false;
                }
                if ($replace_everywhere || $is_target_page){
                    foreach ($GLOBALS['SEOSHIELD_CONFIG']['replace_tag'] as $tag) {
                        if (isset($GLOBALS['SEOSHIELD_CONFIG']['replace_tag_breaks_js']) && $GLOBALS['SEOSHIELD_CONFIG']['replace_tag_breaks_js']){
                            $out_html = str_replace("<".$tag.">","<span class='stag".$tag."'>",$out_html);
                        } else {
                            $out_html = str_replace('<'.$tag.'>', '<span class="stag'.$tag.'">', $out_html);
                        }

                        $out_html = preg_replace_callback('#(<(?:(?!\/)'.$tag.')(?:(?:\s+(?:[\w-]+)(?:\s*=\s*(?:"(?:[^"]*)"|\'(?:[^\']*)\'|(?:[^\'">\s]+)))?)+\s*|\s*)(?:\/)?>)#si', function ($finder_outer) use ($tag, &$out_html) {
                            preg_match_all('#(?<tag_name><'.$tag.'\s)|(?<name>[\w\-]+)\s*=\s*(?:(?:\'(?<value>[^\']*)\')|(?:\"(?<value2>[^\"]*)\"))|(?<e_name>[\w\-]+\s*)#is', $finder_outer[1], $finder_inner);

                            if (isset($finder_inner[0]) && !empty($finder_inner[0])) {
                                $attrs = '';
                                $clss = '';
                                $attrs_a = array();
                                $clss_a = '';

                                foreach ($finder_inner['name'] as $k => $attr_name) {
                                    $attr_name = trim($attr_name);
                                    if (empty($attr_name)) {
                                        $attr_name = trim($finder_inner['e_name'][$k]);
                                    }
                                    if (empty($attr_name)) {
                                        continue;
                                    }
                                    $attr_value = '';
                                    $delimiter = '"';
                                    if (isset($finder_inner['value'][$k]) && !empty($finder_inner['value'][$k])) {
                                        $delimiter = "'";
                                        $attr_value = $finder_inner['value'][$k];
                                    }
                                    if (isset($finder_inner['value2'][$k]) && !empty($finder_inner['value2'][$k])) {
                                        $delimiter = '"';
                                        $attr_value = $finder_inner['value2'][$k];
                                    }

                                    if ('class' == $attr_name) {
                                        $clss_a = $attr_name.'="'.$attr_value.' stag'.$tag.'"';
                                    } else {
                                        $attrs_a[] = $attr_name.'='.$delimiter.$attr_value.$delimiter;
                                    }
                                }

                                $attrs = implode(' ', $attrs_a);
                                $clss = preg_replace(array('#("\s+)#', '#\s+#'), array('"', ' '), $clss_a);
                                if ('' == $clss) {
                                    $clss = 'class="stag'.$tag.'"';
                                }
                            }

                            $result = '<span '.implode(' ', array($attrs, $clss)).'>';
                            $result = preg_replace('#(\s{2,})#s', ' ', $result);

                            return $result;
                        }, $out_html);

                        $out_html = str_replace('</'.$tag.'>', '</span>', $out_html);
                        if (isset($GLOBALS['SEOSHIELD_CONFIG']['safe_tag_prefix']) && !empty($GLOBALS['SEOSHIELD_CONFIG']['safe_tag_prefix'])) {
                            $out_html = str_replace('<'.$GLOBALS['SEOSHIELD_CONFIG']['safe_tag_prefix'].'-'.$tag, '<'.$tag, $out_html);
                            $out_html = str_replace('</'.$GLOBALS['SEOSHIELD_CONFIG']['safe_tag_prefix'].'-'.$tag.'>', '</'.$tag.'>', $out_html);
                        }
                    }

                }
            } else {
                // старая версия
                foreach ($GLOBALS['SEOSHIELD_CONFIG']['replace_tag'] as $tag) {
                    $out_html = str_replace('<'.$tag.'>', '<span class="stag'.$tag.'">', $out_html);
                    $out_html = preg_replace('#<('.$tag." )(((.(?!class))*?)(class *= *[\"']([^\"']*)[\"'])?)?([^>]*)>#is", '<span \\3 \\7 class="\\6 stag'.$tag.'">', $out_html);
                    $out_html = str_replace('</'.$tag.'>', '</span>', $out_html);
                }
            }
        }

        return $out_html;
    }
}
